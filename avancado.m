#include <Foundation/Foundation.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

@interface Trabalho : NSObject
{
  int custoComputacional;
}
- (id) init;
- (void) setCustoComputacional: (int) cc;
- (void) executa;
@end

@implementation Trabalho
- (id) init
{
  self = [super init];
  if (self != nil) return self;
  else return nil;
}

- (void) setCustoComputacional: (int) cc
{
  custoComputacional = cc;
}

- (void) executa
{
  int i;
  double x = 2;
  double y = 10;
  for (i = 0; i < custoComputacional; i++){
    x *= i;
    y *= x;
  }
}
@end

//função que retornar um valor aleatório no intervalo [0.0, 1.0)
double aleatorio ()
{
  return drand48();
}

//função que retorna o tempo atual em segundos
double gettime ()
{
        struct timeval tr;
        gettimeofday(&tr, NULL);
        return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //A lista de argumentos se encontra no arranjo argumentos
  NSArray *argumentos = [[NSProcessInfo processInfo] arguments];

  //Coloque o código deste exercício a partir daqui


  //Executando o laço principal do programa
  [[NSRunLoop currentRunLoop] run];

  //Programa termina
  [pool release];
  return 0;
}
