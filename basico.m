#include <Foundation/Foundation.h>
#include <stdlib.h>

BOOL shouldKeepRunning = YES;

//função que retornar um valor aleatório no intervalo [0.0, 1.0)
double aleatorio ()
{
  return drand48();
}

@interface MinhaClasse : NSObject
{
  //for thread synchronizaton
  NSMutableArray *threads;
  NSCondition *condition;
  int numThreads;

  //for logic
  NSMutableArray *numerosGerados;
  int soma;
}
- (id) init;
- (id) initWithThread: (int) numT;
- (int) soma;

- (void) threadTrabalhadora: (id) object;
- (void) threadPrincipal: (id) object;

@end

@implementation MinhaClasse
- (id) init
{
  self = [super init];
  numerosGerados = [[NSMutableArray alloc] init];
  soma = 0;
  return self;
}

- (id) initWithThread: (int) numT
{
  self = [self init];
  numThreads = numT;
  condition = [[NSCondition alloc] init];
  int i;
  threads = [[NSMutableArray alloc] init];
  for (i = 0; i < numT; i++){
    NSThread *t = [[NSThread alloc] initWithTarget: self selector: @selector(threadTrabalhadora:) object: nil];
    [threads addObject: t];
  }
  NSThread *t = [[NSThread alloc] initWithTarget: self selector: @selector(threadPrincipal:) object: nil];
  [threads addObject: t];

  NSEnumerator *en = [threads objectEnumerator];
  while ((t = [en nextObject])){
    [t start];
  }

  return self;
}

- (int) soma
{
  NSEnumerator *en = [numerosGerados objectEnumerator];
  id numero;
  while ((numero = [en nextObject])){
    soma += [numero integerValue];
  }
  return soma;
}

- (void) dealloc
{
  [numerosGerados release];
  [condition release];
  [threads release];
  [super dealloc];
}

- (void) threadTrabalhadora: (id) object
{
  NSAutoreleasePool *p = [[NSAutoreleasePool alloc] init]; 
  int random = (int)(aleatorio() * 10) + 1;
  @synchronized(self){
    [numerosGerados addObject: [NSNumber numberWithInt: random]];
  }
  [condition lock];
  numThreads--;
  [condition signal];
  [condition unlock];
  [p release];
}

- (void) threadPrincipal: (id) object
{
  NSAutoreleasePool *p = [[NSAutoreleasePool alloc] init]; 
  [condition lock];
  while (numThreads > 0){
    [condition wait];
  }
  NSLog (@"Soma: %d", [self soma]);
  [condition unlock];  
  [p release];
  shouldKeepRunning = NO;
}
@end

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //Coloque o código deste exercício a partir daqui
  NSArray *args = [[NSProcessInfo processInfo] arguments];
  int numThreads = [[args objectAtIndex: 1] intValue];

  //MinhaClasse
  MinhaClasse *minhaClasse = [[MinhaClasse alloc] initWithThread: numThreads];

  //Executando o laço principal do programa
  NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
  [runLoop addPort: [NSPort port] forMode: NSDefaultRunLoopMode];
  while (shouldKeepRunning && [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);

  //Programa termina
  [minhaClasse release];
  [pool release];
  return 0;
}
