#include <Foundation/Foundation.h>

//função que retorna o tempo atual em segundos



//função que calcula a quantidade de linhas de um arquivo
int contadorDeLinhas (NSFileHandle *file)
{
  NSString *entrada = [[[NSString alloc] initWithData: [file availableData]
					     encoding: NSUTF8StringEncoding]
			autorelease];
  return [[entrada componentsSeparatedByString: @"\n"] count];
}

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //A lista de argumentos se encontra no arranjo argumentos
  NSArray *argumentos = [[NSProcessInfo processInfo] arguments];

  //Coloque o código deste exercício a partir daqui


  //Executando o laço principal do programa
  [[NSRunLoop currentRunLoop] run];

  //Programa termina
  [pool release];
  return 0;
}
