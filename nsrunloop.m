#include <Foundation/Foundation.h>
#include <stdio.h>
#include <sys/time.h>
double gettime ()
{
        struct timeval tr;
        gettimeofday(&tr, NULL);
        return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}

@interface Mostrador : NSObject
- (void) mostra: (id) objeto;
@end

@implementation Mostrador
- (void) mostra: (id) objeto
{
  NSLog (@"%s: O objeto eh %@", __FUNCTION__, objeto);
}
@end

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //Instanciando um objeto da classe Mostrador
  Mostrador *mostrador = [Mostrador new];

  //Supondo que a região de código seguinte é considerada crítica
  @synchronized(mostrador) {
    //Instanciando um objeto qualquer
    NSMutableArray *array = [NSMutableArray array];
    [array addObject: @"str1"];
    [array addObject: @"str2"];

    //Lançando um método após 2.5 segundos
    [mostrador performSelector: @selector(mostra:) withObject: array afterDelay: 2.5];
  }

  //Executando o laço principal do programa
  [[NSRunLoop currentRunLoop] run];

  //Programa termina
  [pool release];
  return 0;
}
